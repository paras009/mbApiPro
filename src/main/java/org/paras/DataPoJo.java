package org.paras;


import javax.persistence.Entity;
import javax.persistence.Id;


@Entity(name="tppmt")
public class DataPoJo {

	@Id
	private int PMTRFNUM;
	private String MODIDATE;
	
	
	public DataPoJo(int pMTRFNUM, String mODIDATE, DataPoJo2 pojo2) {
		super();
		PMTRFNUM = pMTRFNUM;
		MODIDATE = mODIDATE;
	}
	
	public DataPoJo() {}



	public int getPMTRFNUM() {
		return PMTRFNUM;
	}


	public String getMODIDATE() {
		return MODIDATE;
	}


}
