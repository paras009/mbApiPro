package org.paras;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="tpcat")
@Table(name="tpcat")
public class DataPoJo2 {

	@Id
	private int CATPMTRFNUM;
	private String CATISVERIFIED;
	private String CATVERIFIEDDATE;
	private String CATVERIFICATIONCOMMENTS;

	
	public DataPoJo2(int cATPMTRFNUM, String cATISVERIFIED, String cATVERIFIEDDATE, String cATVERIFICATIONCOMMENTS) {
		super();
		CATPMTRFNUM = cATPMTRFNUM;
		CATISVERIFIED = cATISVERIFIED;
		CATVERIFIEDDATE = cATVERIFIEDDATE;
		CATVERIFICATIONCOMMENTS = cATVERIFICATIONCOMMENTS;
	}


	public DataPoJo2() {}


	public int getCATPMTRFNUM() {
		return CATPMTRFNUM;
	}


	public String getCATISVERIFIED() {
		return CATISVERIFIED;
	}


	public String getCATVERIFIEDDATE() {
		return CATVERIFIEDDATE;
	}


	public String getCATVERIFICATIONCOMMENTS() {
		return CATVERIFICATIONCOMMENTS;
	}
	



	
	

}
