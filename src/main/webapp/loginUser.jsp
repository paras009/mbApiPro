<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

<title>Insert title here</title>
</head>

<style type="text/css">
	.table {
    border-radius: 5px;
    width: 50%;
    margin: 0px auto;
    margin-top: 150px;
    float: none;
}

  h3{
    margin: 50px;
    }
	
	
</style>
<body>

<% 
String[] id = request.getAttribute("propId").toString().split(","); 
String[] date = request.getAttribute("date").toString().split(",");
String[] isVarified = request.getAttribute("isVarified").toString().split(",");
String[] varifiedDate = request.getAttribute("varifiedDate").toString().split(",");
String[] comment = request.getAttribute("comment").toString().split(",");
%>

<%-- <h3>Details of: ${propId}<br>
</h3>
 --%>




<table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Property Id</th>
      <th scope="col">Modified Date</th>
      <th scope="col">Is Varified</th>
      <th scope="col">Varified Date</th>
      <th scope="col">comments Date</th>
    </tr>
  </thead>
  
  <tbody>
    <% for(int k=0; k<id.length; k++){ %>
    <tr>
      <th scope="row"><%=id[k] %></th>
      <td><%=date[k] %></td>
      <td><%=isVarified[k] %></td>
      <td><%=varifiedDate[k] %></td>
      <td><%=comment[k] %></td>
    </tr>
    <%} %>

  </tbody>
</table>






</body>
</html>